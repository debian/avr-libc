#!/bin/bash

if [ $# -ne 3 ]; then 
    echo Usage: $0 '<tar archive>' '<zip archive>' '<version>'
    echo Exmpl: $0 avr-libc.tar.bz2 avr8-headers.zip 2.0.0+Atmel3.6.0
    exit -1
fi

VER=$3

set -x
set -e

git checkout upstream
rm -Rf libc avr
tar xf $1
unzip $2
git add libc avr
git commit -m "Import upstream version $VER"
git tag upstream/$VER
git push
git push --tags

git checkout master
git merge --no-commit upstream || true
#sed -i -s 's/^MAN_EXTENSION.*/MAN_EXTENSION = .3avr/' libc/avr-libc/doc/api/doxygen.config.in
#echo 'MAN_SUBDIR = man3' >> libc/avr-libc/doc/api/doxygen.config.in
#sed -i -s 's/man\/man3\/\*.3/man\/man3\/\*.3avr/' libc/avr-libc/doc/api/Makefile.am
mv avr/*.h libc/avr-libc/include/avr/
git add libc
git rm -r avr
git commit -m "Merge patched upstream version $VER"

dch -v 1:$VER-1 New upstream release
dch -r ok
git add debian/changelog

vim debian/control
git add debian/control

git commit -m 'Release message in changelog and new versioned deps'

git tag "debian/$VER-1"
git push
git push --tags

gbp buildpackage --git-pbuilder --git-upstream-tree=branch --git-upstream-branch=upstream
