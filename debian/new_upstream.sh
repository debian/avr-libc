#!/bin/bash

if [ $# -ne 1 ]; then 
    echo Usage: $0  '<version>'
    echo Exmpl: $0 2.0.0+Atmel3.5.4
    exit -1
fi


mkdir ../avr-libc-$1".orig"
cp  avr8-headers.zip avr-libc.tar.bz2 ../avr-libc-$1".orig"
dch -v "1:"$1"-1" New upstream release
dch -r ok
echo
echo Now update debian/control
echo
